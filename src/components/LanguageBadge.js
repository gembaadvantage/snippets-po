import styles from "./LanguageBadge.module.css";

const LanguageColors = {
  js: "#ec008c",
  go: "#7fd5ea",
  py: "#ffd343",
};

function LanguageBadge({ language }) {
  if (!language) {
    return null;
  }

  const { key, displayName } = language;

  const color = {
    backgroundColor: LanguageColors[key],
  };

  return (
    <div className={styles.Badge}>
      <div className={styles.Dot} style={color} />
      <span className="mdc-typography--caption">{displayName}</span>
    </div>
  );
}

export default LanguageBadge;
