import { useEffect, useState } from "react";
import { useFetch } from "../api/useFetch.hook";
import SnippetCard from "../components/SnippetCard";

function SnippetCardContainer({ snippetId }) {
  const [makeRequest, { data }] = useFetch();

  // See https://github.com/typicode/json-server#relationships
  // This expands the author and language objects so we
  // don't have to fetch them by id subsequently
  const [endpoint] = useState(
    `/snippets/${snippetId}/?_expand=author&_expand=language`
  );

  useEffect(() => makeRequest(endpoint), [makeRequest, endpoint]);

  if (data) {
    return <SnippetCard {...data} key={data.id} />;
  }

  return null;
}

export default SnippetCardContainer;
