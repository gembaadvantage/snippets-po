import { useEffect, useState } from "react";
import { useFetch } from "../api/useFetch.hook";
import SnippetCardContainer from "./SnippetCard.container";
import styles from "./Snippets.module.css";

function SnippetsContainer() {
  const [makeRequest, { data, error }] = useFetch();

  const [endpoint] = useState("/snippets");

  // Initial fetch
  useEffect(() => {
    makeRequest(endpoint);
  }, [makeRequest, endpoint]);

  if (error) {
    // TODO: Display error nicely
    return <div>Error!</div>;
  }

  if (data) {
    return (
      <section className={styles.Container}>
        {data.map((snippet) => (
          <SnippetCardContainer snippetId={snippet.id} key={snippet.id} />
        ))}
      </section>
    );
  }

  return null;
}

export default SnippetsContainer;
